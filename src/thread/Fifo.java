package thread;

import java.util.LinkedList;

public class Fifo {

	private int n;
	private LinkedList<String> list;

	public Fifo() {
		this.n = 0;
		this.list = new LinkedList<String>();
	}

	public synchronized void put(String string) throws InterruptedException {

		while (!(n < 10)) {
			this.wait();
		}
		
		System.out.println("put " + Thread.currentThread().getId());

		this.notifyAll();
		++this.n;
		this.list.add(string);
	}

	public synchronized String get() throws InterruptedException {

		while (!(n > 0)) {
			this.wait();
		}
		
		System.out.println("get " + Thread.currentThread().getId());

		this.notifyAll();
		--this.n;
		return this.list.pop();
	}

}
