package thread;

public class Producer implements Runnable {

	private int count;

	private Fifo fifo;
	private String name;
	private int waitTime;

	public Producer(Fifo fifo, String name, int waitTime) {

		this.count = 0;

		this.fifo = fifo;
		this.name = name;
		this.waitTime = waitTime;

	}

	@Override
	public void run() {

		String message = this.name + " " + this.count++;
		try {
			this.fifo.put(message);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		String time = String.valueOf(System.currentTimeMillis());
		System.out.println("produced " + message + " " + time.substring(time.length() - 5));

		try {
			Thread.sleep(this.waitTime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
}
