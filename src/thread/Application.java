package thread;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class Application {

	public static void main(String[] args) {
		
		
		// Nemigazán értem, hogy ebben a feladatban hogyan kéne időzíteni a dolgokat, de remélem, hogy így jó lesz.
		ScheduledExecutorService producers = Executors.newScheduledThreadPool(10);
		ScheduledExecutorService consumers = Executors.newScheduledThreadPool(10);

		Fifo f1 = new Fifo();
		
		for(int i = 0; i < 15; ++i) {
			producers.execute(new Producer(f1, "P" + i, 1600));
			consumers.execute(new Consumer(f1, "C" + i, 1600));
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
		
		producers.shutdown();
		consumers.shutdown();
	}
}
