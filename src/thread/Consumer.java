package thread;

public class Consumer implements Runnable {

	private Fifo fifo;
	private String name;
	private int waitTime;

	public Consumer(Fifo fifo, String name, int waitTime) {

		this.fifo = fifo;
		this.name = name;
		this.waitTime = waitTime;

	}

	@Override
	public void run() {

		String message = null;
		try {
			message = this.fifo.get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		String time = String.valueOf(System.currentTimeMillis());
		System.out.println("consumed " + this.name + " " + message + " " + time.substring(time.length() - 5));

		try {
			Thread.sleep(this.waitTime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
}
